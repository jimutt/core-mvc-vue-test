import Vue from 'vue';

const StaticTextComponent = () =>
  import(
    /* webpackChunkName: 'static-text' */
    './components/StaticTextComponent.vue'
  )

const LeafletComponent = () =>
  import(
    /* webpackChunkName: 'leaflet-map' */
    './components/LeafletComponent.vue'
  )

const AxiosComponent = () =>
  import(
    /* webpackChunkName: 'axios' */
    './components/AxiosComponent.vue'
  )

new Vue({
  el: '#app',
  components: {
    StaticTextComponent,
    LeafletComponent,
    AxiosComponent
  }
});