﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace MvcVueTest.Controllers
{
  public class HomeController : Controller
  {
    public IActionResult Index()
    {
      return View();
    }

    public IActionResult AllComponents()
    {
      return View();
    }

    public IActionResult LeafletComponent()
    {
      return View();
    }

    public IActionResult StaticTextComponent()
    {
      return View();
    }

    public IActionResult AxiosComponent()
    {
      return View();
    }
  }
}
