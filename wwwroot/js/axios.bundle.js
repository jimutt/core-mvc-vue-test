(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["axios"],{

/***/ "./node_modules/vue-loader/lib/index.js?!./scripts/components/AxiosComponent.vue?vue&type=script&lang=js&":
/*!**************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib??vue-loader-options!./scripts/components/AxiosComponent.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n//\n//\n//\n//\n//\n//\n//\n//\n\nconst axios = __webpack_require__(/*! axios */ \"./node_modules/axios/index.js\");\n\n/* harmony default export */ __webpack_exports__[\"default\"] = ({\n  data() {\n    return {};\n  },\n  methods: {\n    async dummyClick() {\n      await axios.get('');\n    }\n  }\n});\n\n\n//# sourceURL=webpack:///./scripts/components/AxiosComponent.vue?./node_modules/vue-loader/lib??vue-loader-options");

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./scripts/components/AxiosComponent.vue?vue&type=template&id=110576fb&":
/*!************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./scripts/components/AxiosComponent.vue?vue&type=template&id=110576fb& ***!
  \************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"render\", function() { return render; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"staticRenderFns\", function() { return staticRenderFns; });\nvar render = function() {\n  var _vm = this\n  var _h = _vm.$createElement\n  var _c = _vm._self._c || _h\n  return _c(\"div\", { staticClass: \"component\" }, [\n    _c(\"h2\", [_vm._v(\"Axios component\")]),\n    _vm._v(\" \"),\n    _c(\"p\", [\n      _vm._v(\n        \"This component doesn't do anything. It just declares a dependency to the Axios library to show the impact on the vendor bundle. \"\n      )\n    ]),\n    _vm._v(\" \"),\n    _c(\"button\", { on: { click: _vm.dummyClick } }, [\n      _vm._v(\"Send empty axios GET request\")\n    ])\n  ])\n}\nvar staticRenderFns = []\nrender._withStripped = true\n\n\n\n//# sourceURL=webpack:///./scripts/components/AxiosComponent.vue?./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options");

/***/ }),

/***/ "./scripts/components/AxiosComponent.vue":
/*!***********************************************!*\
  !*** ./scripts/components/AxiosComponent.vue ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _AxiosComponent_vue_vue_type_template_id_110576fb___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./AxiosComponent.vue?vue&type=template&id=110576fb& */ \"./scripts/components/AxiosComponent.vue?vue&type=template&id=110576fb&\");\n/* harmony import */ var _AxiosComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./AxiosComponent.vue?vue&type=script&lang=js& */ \"./scripts/components/AxiosComponent.vue?vue&type=script&lang=js&\");\n/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ \"./node_modules/vue-loader/lib/runtime/componentNormalizer.js\");\n\n\n\n\n\n/* normalize component */\n\nvar component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__[\"default\"])(\n  _AxiosComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__[\"default\"],\n  _AxiosComponent_vue_vue_type_template_id_110576fb___WEBPACK_IMPORTED_MODULE_0__[\"render\"],\n  _AxiosComponent_vue_vue_type_template_id_110576fb___WEBPACK_IMPORTED_MODULE_0__[\"staticRenderFns\"],\n  false,\n  null,\n  null,\n  null\n  \n)\n\n/* hot reload */\nif (false) { var api; }\ncomponent.options.__file = \"scripts/components/AxiosComponent.vue\"\n/* harmony default export */ __webpack_exports__[\"default\"] = (component.exports);\n\n//# sourceURL=webpack:///./scripts/components/AxiosComponent.vue?");

/***/ }),

/***/ "./scripts/components/AxiosComponent.vue?vue&type=script&lang=js&":
/*!************************************************************************!*\
  !*** ./scripts/components/AxiosComponent.vue?vue&type=script&lang=js& ***!
  \************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _node_modules_vue_loader_lib_index_js_vue_loader_options_AxiosComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../node_modules/vue-loader/lib??vue-loader-options!./AxiosComponent.vue?vue&type=script&lang=js& */ \"./node_modules/vue-loader/lib/index.js?!./scripts/components/AxiosComponent.vue?vue&type=script&lang=js&\");\n/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__[\"default\"] = (_node_modules_vue_loader_lib_index_js_vue_loader_options_AxiosComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__[\"default\"]); \n\n//# sourceURL=webpack:///./scripts/components/AxiosComponent.vue?");

/***/ }),

/***/ "./scripts/components/AxiosComponent.vue?vue&type=template&id=110576fb&":
/*!******************************************************************************!*\
  !*** ./scripts/components/AxiosComponent.vue?vue&type=template&id=110576fb& ***!
  \******************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_AxiosComponent_vue_vue_type_template_id_110576fb___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../node_modules/vue-loader/lib??vue-loader-options!./AxiosComponent.vue?vue&type=template&id=110576fb& */ \"./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./scripts/components/AxiosComponent.vue?vue&type=template&id=110576fb&\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"render\", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_AxiosComponent_vue_vue_type_template_id_110576fb___WEBPACK_IMPORTED_MODULE_0__[\"render\"]; });\n\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"staticRenderFns\", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_AxiosComponent_vue_vue_type_template_id_110576fb___WEBPACK_IMPORTED_MODULE_0__[\"staticRenderFns\"]; });\n\n\n\n//# sourceURL=webpack:///./scripts/components/AxiosComponent.vue?");

/***/ })

}]);