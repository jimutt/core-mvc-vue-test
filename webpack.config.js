const VueLoaderPlugin = require('vue-loader/lib/plugin')
const Visualizer = require('webpack-visualizer-plugin');
const path = require('path')

module.exports = {
  mode: 'development',
  entry: [
    './scripts/app.js'
  ],
  output: {
    publicPath: '/js/',
    chunkFilename: '[name].bundle.js',
    path: path.resolve(__dirname, 'wwwroot/js')
  },
  module: {
    rules: [{
        test: /\.vue$/,
        use: 'vue-loader'
      },
      {
        test: /\.css$/,
        use: [
          'vue-style-loader',
          'css-loader'
        ]
      }
    ]
  },
  plugins: [
    new VueLoaderPlugin(),
    new Visualizer({
      filename: '../statistics.html'
    })
  ],
  optimization: {
    splitChunks: {
      chunks: 'all'
    }
  },
  resolve: {
    alias: {
      'vue$': 'vue/dist/vue.esm.js'
    }
  }
}